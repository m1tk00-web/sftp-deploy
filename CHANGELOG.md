# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.2

- patch: Internal maintenance: Refactor tests to python.

## 0.5.1

- patch: Internal maintenance: Automate test infrastructure.

## 0.5.0

- minor: Add variable PASSWORD as an alternative authentication method.

## 0.4.1

- patch: Update pipes bash toolkit version.

## 0.4.0

- minor: LOCAL_PATH had become an optional parameter

## 0.3.1

- patch: Standardising README and pipes.yml.

## 0.3.0

- minor: Force the git ssh command used when pushing a new tag to override the default SSH configuration for piplines.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.3

- patch: Fix use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines SFTP pipe.

